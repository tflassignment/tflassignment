# TFL iOS Assignment
 
## Requirements:
 
 We would like you to build a simple iOS app that should work as follows:
 
 1) Given a valid road ID is input in the app
 
 - Then the road ‘displayName’ should be displayed.
 
 2) Given a valid road ID is input in the app
 
 - Then the road ‘statusSeverity’ should be displayed as ‘Road Status.
 
 3) Given a valid road ID is input in the app
 
 - Then the road ‘statusSeverityDescription’ should be displayed as ‘Road Status Description’.
 
 4) Given an invalid road ID is input in the app
 
 - Then the application should display an informative error
 
## Approach:
 
 1) TDD approach is used. However UI test are also written
 
 2) Swift 4.1 is used.
 
 3) No open source library is used for development except SwiftLint. Explanation for SwiftLint is given below
 
## how to run and app out:
 
 1) How to build and run the code
 
 - Clone the repository and  open it in the latest Xcode version.
 
 - Make sure appid' and 'appkey' are set in the 'APIConstant.swift' before running the app in simulator / iPhone
 
 - Once the app is launched successfully.  Enter the road name in the text field to fetch the road data for given road name or just tap search without entering the road name this will fetch all the roads data from the service.
 
 - If service returns with any error, it will be displayed
 
 2) How to run any tests that you have written
 
 - Test will run once the 'appId' and 'appkey' are set in the APIConstant
 
 - Unit, UI Test are included
 
 3) any assumptions that you’ve made
 
 - If no road name is entered in the text field and search button is pressed. All the roads data will be displayed in the tableview
 
 4) any anomalies you find with the API and how to address them
 
 - API was easy to understand and implement
 
 5) anything else you think is relevant
 
 - As mentioned in the above point; if no road name is entered in the text field and search button is pressed. All the roads data will be displayed in the tableview
 
##  Used Open Source Libraries
 
 1) SwiftLint (A tool to enforce Swift style and conventions, loosely)
