//
//  TestViewController.swift
//  TFLAssignmentTests
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import XCTest
@testable import TFLAssignment

class TestViewController: XCTestCase {
    var roadVC: RoadVC!
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        let storyboard = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil)
        guard let roadViewController =
            storyboard.instantiateViewController (withIdentifier: StoryboardIdentity.roadVC.rawValue) as? RoadVC else {
            XCTFail("RoadViewController can't be nil")
            return
        }
        roadVC = roadViewController
        roadVC.loadViewIfNeeded()
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testMatchViewController() {
        XCTAssertNotNil(roadVC.tableView, "tableView can't be nill")
    }
}
