//
//  TFLAssignmentTests.swift
//  TFLAssignmentTests
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import XCTest
@testable import TFLAssignment

class TFLAssignmentTests: XCTestCase, ApplicationConfiguration {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testApplicationConfiguration_settings() {
        if !isApplicationConfigured() {
            XCTFail("Please set application variable in APIConstant. To Perform Unit Test")
        }
    }
    func testRoadDataManager_fetchRoad() {
        //Given
        let exp = expectation(description: "Wait for RoadDataManager completion")
        let request = RoadRequest(endpoint: .roadName(name: "A2"))
        //WHEN
        RoadDataManager.shared.fetchRoadList(request: request) { (result) in
            switch result {
            case .success: //let data
                exp.fulfill()
                SharedLogger.logInfo("Road count: \(RoadDataManager.shared.numberOfRows())")
            case .failure(let error): //
                switch error {
                case .responseError(let errorStatus):
                    if let errorStatus = errorStatus {
                        XCTFail(errorStatus.message)
                    } else {
                        XCTFail(error.description)
                    }
                default:
                    break
                }
            }
        }
        //Then
        waitForExpectations(timeout: 20, handler: { error in
            if let error = error {
                XCTFail("Error while waiting: \(error)")
            }
        })
        /*
         We are expecting a Road information from the service.
         To Pass the test there should be min 1 road information in response.
         */
        let count = RoadDataManager.shared.numberOfRows()
        XCTAssertEqual(count, 1,
                       "Expected 1 Road, returned \(String(describing: count))")
        //WHEN
        RoadDataManager.shared.clear()
        let clearCount = RoadDataManager.shared.numberOfRows()
        //THEN
        XCTAssertEqual(clearCount, 0,
                       "Expected 0 Road after clearing, still showning \(clearCount) Road)")
    }
    func testRoadDataManager_nonExistentRoad() {
        //Given
        let exp = expectation(description: "Wait for RoadDataManager completion")
        let request = RoadRequest(endpoint: .roadName(name: "A2232"))
        //WHEN
        RoadDataManager.shared.fetchRoadList(request: request) { (result) in
            switch result {
            case .success: //let data
                SharedLogger.logInfo("Road count: \(RoadDataManager.shared.numberOfRows())")
                XCTFail("This should get 404")
            case .failure: //
                exp.fulfill()
            }
        }
        //Then
        waitForExpectations(timeout: 20, handler: { error in
            if let error = error {
                XCTFail("Error while waiting: \(error)")
            }
        })
        /*
         We are expecting no Road information from the service.
         To Pass the test there should be 404 in response status.
         */
        let count = RoadDataManager.shared.numberOfRows()
        XCTAssertEqual(count, 0,
                       "Expected 0 Road, returned \(String(describing: count))")
    }
    func testRoadDataManager_fetchAllRoad() {
        //Given
        let exp = expectation(description: "Wait for RoadDataManager completion")
        let request = RoadRequest(endpoint: .roadName(name: ""))
        //WHEN
        RoadDataManager.shared.fetchRoadList(request: request) { (result) in
            switch result {
            case .success: //let data
                exp.fulfill()
                SharedLogger.logInfo("Road count: \(RoadDataManager.shared.numberOfRows())")
            case .failure(let error): //
                switch error {
                case .responseError(let errorStatus):
                    if let errorStatus = errorStatus {
                        XCTFail(errorStatus.message)
                    } else {
                        XCTFail(error.description)
                    }
                default:
                    break
                }
            }
        }
        //Then
        waitForExpectations(timeout: 20, handler: { error in
            if let error = error {
                XCTFail("Error while waiting: \(error)")
            }
        })
        /*
         We are expecting a Road information from the service.
         To Pass the test there should be more the 1 road information in response.
         */
        let count = RoadDataManager.shared.numberOfRows()
        XCTAssertGreaterThan(count, 1,
                             "Expected more then 1 Road, returned \(String(describing: count))")
        //WHEN
        RoadDataManager.shared.clear()
        let clearCount = RoadDataManager.shared.numberOfRows()
        //THEN
        XCTAssertEqual(clearCount, 0,
                       "Expected 0 Road after clearing, still showning \(clearCount) Road)")
    }
}
