//
//  TFLAssignmentUITests.swift
//  TFLAssignmentUITests
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import XCTest

class TFLAssignmentUITests: XCTestCase, ApplicationConfiguration {
    let app =  XCUIApplication()
    let typeText = "A2"
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launchArguments.append("--uitesting")  //
        app.launch()
        _ = waitForElementToAppear(app)
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    /// DismissAlert
    ///
    /// - Returns: Bool
    func dismissAlert() -> Bool {
        let cancelButton = app.alerts.buttons["OK"].firstMatch
        if cancelButton.exists {
            cancelButton.tap()
            app.tap()
            return true
        } else {
            return false
        }
    }

    func testRoadViewController() {
        XCTContext.runActivity(named: "RoadVC") { _ in
            if !isApplicationConfigured() {
                XCTFail("Please set application variable in APIConstant. To Perform UI Test")
            }
            let txtRoad: XCUIElement = app.textFields[AccessibilityLabel.txtRoadName.rawValue]
                .firstMatch
            XCTAssertNotNil(txtRoad.exists,
                            "\(AccessibilityLabel.txtRoadName.rawValue) doesn't exists")
            sleep(2)
            txtRoad.tap()
            sleep(2)
            txtRoad.typeText(typeText)
            sleep(2)
            app.keyboards.buttons["Search"].tap()
            let roadTableView: XCUIElement = app.tables[AccessibilityLabel.roadTable.rawValue]
                .firstMatch
            XCTAssertNotNil(roadTableView.exists,
                            "\(AccessibilityLabel.roadTable.rawValue) doesn't exists")
            let cell = roadTableView.cells["\(AccessibilityLabel.roadTableViewCell.rawValue)-0"]
            if !cell.waitForExistence(timeout: 10) {
                if dismissAlert() {
                    XCTFail("some error occurred")
                }
            }
            sleep(5)
        }
    }
}
