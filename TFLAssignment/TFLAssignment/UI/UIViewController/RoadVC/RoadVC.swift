//
//  RoadVC.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import UIKit

class RoadVC: BaseVC, TableViewViewable {
    @IBOutlet weak var txtRoadName: UITextField! {
        didSet {
            txtRoadName.delegate = self
            txtRoadName.accessibilityLabel = AccessibilityLabel.txtRoadName.rawValue
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib.loadNib(named: .road),
                               forCellReuseIdentifier: TableCellReuseIdentifier.road.rawValue)
            tableView.estimatedRowHeight = 80.0
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.dataSource = self
            tableView.tableFooterView = tableViewFooterView()
            tableView.accessibilityLabel = AccessibilityLabel.roadTable.rawValue
            tableView.backgroundView = EmptyMessageView(frame: tableView.bounds)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
}
extension RoadVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        RoadDataManager.shared.clear()
        tableView.reloadData()
        loadData(roadName: txtRoadName.text ?? "")
        return true
    }
}
extension RoadVC {
    /// LoadData
    ///
    /// - Parameter roadName: RoadName as String
    func loadData(roadName: String) {
        showLoader()
        let request = RoadRequest(endpoint: .roadName(name: roadName))
        RoadDataManager.shared.fetchRoadList(request: request) { [weak self]  (result) in
            guard let `self` = self else { return }
            self.hideLoader()
            switch result {
            case .success: //let data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                SharedLogger.logInfo("Road list count: \(RoadDataManager.shared.numberOfRows())")
            case .failure(let error): //
                var errorMessage = ""
                switch error {
                case .responseError(let errorStatus):
                    if let errorStatus = errorStatus {
                        errorMessage = errorStatus.message
                    }
                default:
                    errorMessage = error.description
                }
                DispatchQueue.main.async {
                    UIViewController.notifyUser("", message: errorMessage,
                                                alertButtonTitles: ["OK"],
                                                alertButtonStyles: [.default],
                                                presenterView: self,
                                                completion: { _ in
                    })
                }
            }
        }
    }
}
// MARK: - UITableViewDataSource
extension RoadVC: UITableViewDataSource {
    /// TableView row count
    ///
    /// - Parameters:
    ///   - tableView: tableView
    ///   - section: section
    /// - Returns: number of rows in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rowCount = RoadDataManager.shared.numberOfRows()
        tableView.backgroundView?.isHidden = rowCount > 0
        return rowCount
    }
    /// TableView cell
    ///
    /// - Parameters:
    ///   - tableView: tableView
    ///   - indexPath: indexPath
    /// - Returns: Custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableCellReuseIdentifier.road.rawValue,
                                                       for: indexPath) as? RoadTableViewCell else {
                                                        return UITableViewCell()
        }
        cell.road = RoadDataManager.shared.getRoadAtIndex(index: indexPath.row)
        cell.accessibilityLabel = "\(AccessibilityLabel.roadTableViewCell.rawValue)-\(indexPath.row)"
        return cell
    }
}
