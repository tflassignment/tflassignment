//
//  EmptyMessageView.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import UIKit

class EmptyMessageView: UIView {
    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    private func setUpView() {
        Bundle.main.loadNibNamed("EmptyMessageView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame =  bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
