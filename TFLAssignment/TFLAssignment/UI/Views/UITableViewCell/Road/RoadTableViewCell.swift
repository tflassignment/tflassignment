//
//  RoadTableViewCell.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import UIKit

class RoadTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusDes: UILabel!
    var road: Road? {
        didSet {
            if let selectedRoad = road {
                lblName.text = selectedRoad.displayName
                lblStatus.text = selectedRoad.statusSeverity
                lblStatusDes.text = selectedRoad.statusSeverityDescription
            }
        }
    }
}
