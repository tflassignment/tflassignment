//
//  AppConstant.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

public enum AccessibilityLabel: String {
    case roadTable = "RoadTableViewAccessibility"
    case roadTableViewCell = "RoadTableViewCellAccessibility"
    case txtRoadName = "TxtRoadNameAccessibility"
}

public enum NibName: String {
    case road = "RoadTableViewCell"
}

public enum TableCellReuseIdentifier: String {
    case road = "RoadTableViewCellReuseIdentifier"
}

public enum StoryboardName: String {
    case main = "Main"
}

public enum StoryboardIdentity: String {
    case roadVC = "RoadVC"
}
