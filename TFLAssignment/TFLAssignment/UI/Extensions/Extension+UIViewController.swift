//
//  Extension+UIViewController.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//
import UIKit
extension UIViewController {
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - title: title
    ///   - message: message
    ///   - alertButtonTitles: [alertButtonTitles]
    ///   - alertButtonStyles: [alertButtonStyles]
    ///   - presenterView: UIViewController
    ///   - completion: Selected Index
    // swiftlint:disable:next function_parameter_count
    static func notifyUser(_ title: String, message: String,
                           alertButtonTitles: [String],
                           alertButtonStyles: [UIAlertActionStyle],
                           presenterView: UIViewController,
                           completion: @escaping (Int) -> Void) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        for title in alertButtonTitles {
            let actionObj = UIAlertAction(title: title,
                                          style: alertButtonStyles[alertButtonTitles.index(of: title)!],
                                          handler: { action in
                                            completion(alertButtonTitles.index(of: action.title!)!)
            })
            alert.addAction(actionObj)
        }
        //vc will be the view controller on which you will,
        //present your alert as you cannot use self because this method is static.
        presenterView.present(alert, animated: true, completion: nil)
    }
    /// ShowLoader on the view
    public func showLoader() {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            ProgressView.sharedInstance.showLoader(self.view)
        }
    }
    /// HideLoader from the view
    public func hideLoader() {
        DispatchQueue.main.async { [weak self] in
            guard `self` != nil else { return }
            ProgressView.sharedInstance.hideLoader()
        }
    }
}
