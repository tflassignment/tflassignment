//
//  TableViewViewable.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import UIKit

protocol TableViewViewable {
    func tableViewFooterView() -> UIView
}
extension TableViewViewable {
    func tableViewFooterView() -> UIView {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
}
