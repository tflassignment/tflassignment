//
//  ApplicationConfiguration.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 13/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

protocol ApplicationConfiguration {
    func isApplicationConfigured() -> Bool
}

extension ApplicationConfiguration {
    func isApplicationConfigured () -> Bool {
        return (APIConstant.appId.lengthOfBytes(using: .utf8) > 0 && APIConstant.appkey.lengthOfBytes(using: .utf8) > 0)
    }
}
