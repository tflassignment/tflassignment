//
//  AppDelegate.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ApplicationConfiguration {

    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if !isApplicationConfigured() {
            preconditionFailure("""
Please set application variable in APIConstant.swift. Variable needs to be set  are 'appId and appkey'
""")

        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}
