//
//  Road.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

struct Road: Codable {
    let type, roadId, displayName, statusSeverity: String
    let statusSeverityDescription, url: String
}

// MARK: - Road
extension Road {
    enum CodingKeys: String, CodingKey {
        case type = "$type"
        case displayName, statusSeverity, statusSeverityDescription, url
        case roadId = "id"
    }

    /// Decoding Response
    ///
    /// - Parameter decoder: Decoder
    /// - Throws: Error
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        roadId = try container.decode(String.self, forKey: .roadId)
        displayName = try container.decode(String.self, forKey: .displayName)
        statusSeverity = try container.decode(String.self, forKey: .statusSeverity)
        statusSeverityDescription = try container.decode(String.self, forKey: .statusSeverityDescription)
        url = try container.decode(String.self, forKey: .url)
    }
}
