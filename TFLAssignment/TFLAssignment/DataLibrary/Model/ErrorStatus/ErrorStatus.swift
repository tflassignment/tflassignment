//
//  ErrorStatus.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

struct ErrorStatus: Decodable {
    let type, timestampUTC, exceptionType: String
    let httpStatusCode: Int
    let httpStatus, relativeURI, message: String
}
// MARK: - ErrorStatus
extension ErrorStatus {
    enum CodingKeys: String, CodingKey {
        case type = "$type"
        case timestampUTC = "timestampUtc"
        case exceptionType, httpStatusCode, httpStatus
        case relativeURI = "relativeUri"
        case message
    }
    /// Decoding Response
    ///
    /// - Parameter decoder: Decoder
    /// - Throws: Error
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        timestampUTC = try container.decode(String.self, forKey: .timestampUTC)
        exceptionType = try container.decode(String.self, forKey: .exceptionType)
        httpStatusCode = try container.decode(Int.self, forKey: .httpStatusCode)
        httpStatus = try container.decode(String.self, forKey: .httpStatus)
        relativeURI = try container.decode(String.self, forKey: .relativeURI)
        message = try container.decode(String.self, forKey: .message)
    }
}
