//
//  RoadDataManager.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//
import Foundation
public final class RoadDataManager: BaseDataManager {
    static let shared = RoadDataManager()
    fileprivate var roadData: [Road] = []
    private init() { }
}
extension RoadDataManager {
    /// Clear the data
    func clear() {
        roadData.removeAll()
    }
    /// NumberOfRows
    ///
    /// - Returns: number of rows
    func numberOfRows() -> Int {
        return roadData.count
    }
    /// getRoadAtIndex
    ///
    /// - Parameter index: index (0...)
    /// - Returns: Road
    func getRoadAtIndex(index: Int) -> Road {
        return roadData[index]
    }
}
extension RoadDataManager {
    /// FetchRoadList
    ///
    /// - Parameters:
    ///   - request: RoadRequest
    ///   - completion: (HttpRequestResult<Bool?, HttpRequestError>)
    func fetchRoadList(request: RoadRequest,
                       completion: @escaping (HttpRequestResult<Bool?, HttpRequestError>) -> Void) {
        let client = RoadService(withRequest: request)
        client.fetchRoadData { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let dataResponse):
                guard let dataResponse = dataResponse, dataResponse.count > 0 else {
                    return completion(.failure(.message(message: "No data in response")))
                }
                self.roadData = dataResponse
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
