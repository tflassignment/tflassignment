//
//  RoadService.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

public enum RoadEndpoint {
    case roadName(name: String)
}

extension RoadEndpoint: Endpoint {
    var path: String {
        switch self {
        case .roadName(let name): return name
        }
    }
}

public struct RoadRequest: BaseClientRequest {
    let roadEndpoint: RoadEndpoint
    /// Init request with endpoint
    ///
    /// - Parameter endpoint: RoadEndpoint
    public init(endpoint: RoadEndpoint) {
        roadEndpoint = endpoint
    }
}

class RoadService: HttpRequest {
    var query: HttpRequestQueryType {
        return .path
    }
    ///desired Response Type
    typealias ResponseObject = [Road]
    internal let endpoint: Endpoint
    private let roadRequest: RoadRequest
    /// Initialize service with request
    ///
    /// - Parameter request: RoadRequest
    init(withRequest request: RoadRequest) {
        roadRequest = request
        endpoint = request.roadEndpoint
    }
    /// fetchRoad
    ///
    /// - Parameter completion: HttpRequestResult<ResponseObject?, HttpRequestError>
    func fetchRoadData(completion: @escaping (HttpRequestResult<ResponseObject?, HttpRequestError>) -> Void) {
        sendRequest(decode: { json -> ResponseObject? in
            guard let result = json as? ResponseObject else { return  nil }
            return result
        }, completion: completion)
    }
}
