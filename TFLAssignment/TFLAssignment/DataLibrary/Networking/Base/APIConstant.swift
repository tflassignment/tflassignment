//
//  APIConstant.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

struct APIConstant {
    private init() {}
    static let appId = ""
    static let appkey = ""
}
