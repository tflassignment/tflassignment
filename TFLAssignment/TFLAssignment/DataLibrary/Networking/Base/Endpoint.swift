//
//  Endpoint.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation

protocol Endpoint {
    var path: String { get }
}

extension Endpoint { }
