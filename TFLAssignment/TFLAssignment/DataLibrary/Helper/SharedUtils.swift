//
//  SharedUtils.swift
//  TFLAssignment
//
//  Created by Yasir Basharat on 12/05/2018.
//  Copyright © 2018 Yasir Basharat. All rights reserved.
//

import Foundation
open class SharedUtils {
    private init() { }
    /// Return non-optional string
    ///
    /// - Parameter value: Optional String
    /// - Returns: String
    static func extractStringValue(_ value: String?) -> String {
        if let str = value {
            return str
        }
        return ""
    }
    /// To get installed application path
    ///
    /// - Returns: Path of the application
    static func displayApplicationPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
    }
}
